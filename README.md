# Sweet spot for therapeutic intervention for COVID-19

This repository contains Python scripts for "Between viral targets and differentially expressed genes in COVID-19: the sweet spot for therapeutic intervention" by Carme Zambrana, Alexandros Xenos, René Bötcher, Noël Malod-Dognin, and Nataša Pržulj.

In this work, we adapt our GNMTF-based data fusion framework to predicted candidate target genes and existing drugs that could be re-purposed for
treating COVID-19. Moreover, we investigate within the human interactome the interplay between the human proteins that are directly targeted by the SARS-CoV-2 proteins and those genes that are differentially expressed after COVID-19 infection. Our study reveals that the host proteins targeted by viral proteins and the differentially expressed genes are indirectly connected by their neighbors (we termed common neighbor genes). Furthermore, we find that the common neighbors are enriched in various viral processes and hence, might be key to the infection mechanisms used by the virus. By focusing on the predicted drug-target interactions involving FDA-approved drugs and targeting the common neighbor genes, we utilize our integrative framework to predict novel drug-target interactions for genes related to the disease-affected pathways. In particular, we find NO and VEGF signaling as potential molecular pathways whose functions are very similar with several observed COVID-19 symptoms.

Corresponding author: Prof. N. Przulj, e-mail: natasha@bsc.es

-------------------------------------------------------------------------------------------------------------------------------
Workflow detailing the steps of the analysis and the notebooks with the corresponding code

![alt text](<Workflow.png>)

-------------------------------------------------------------------------------------------------------------------------------


The execution of all the scripts is detailed in the following notebooks:

    - Executing data fusion framework - Parametrization Tunning : 
        Executes the scripts to create/load matrices and toexecute the data fusion framework with different parametrization using Random Acol for initializing the matrices.
        
    - Executing data fusion framework - Parametrization Tunning - Dispersion Coefficient :
         Executes the scripts to compute the dispersion coefficient after computing the data fusion with the different parameters.
         
    - Executing data fusion framework :
        Executes the scripts to create/load the matrices and to execute the data fusion framework with SVD initialization after choosing the parameters.
        
    - Enrichment Analysis of the Clusters of Genes and Drugs - MIN and PPI :
        Executes the scripts to compute the cluster from the clusters indicator and to perform the enrichment analysis of those clusters.
        
    - Matrix Completion Property of the DTI matrix - Choosing the threshold - PR and ROC - MIN and PPI: 
        Executes the scripts to reconstruct the DTI matrix through the matrix completition property, to compute PR and ROC curves and to choose the threshold to obtain the new predicted DTIs.
        
    - Matrix Completion Property of the DTI matrix - Cross-Validation - MIN and PPI:
        Executes the cross-validation experiment to validate the predictive power of the method. 
        
    - Topological Analysis Network :
        Executes the scripts to identify the different gene sets, to compute the overlap between VI neighbors and DEG neighbors, and to compute the network centralities and the GDVs for the different gene sets.
        
    - Enrichment Analysis Gene Sets in MIN :
        Executes the scripts to compute the enrichment analysis of the different gene sets in the MIN and the 49 common neighbors targeted in the "common neighbors DTIs" using the Gprofiler.
        
     - Comparison constituent networks (PPI, GI MI) with MIN :
        Executes the scripts to compare the genes and edges of the constituent networks with the MIN networks and to compute the GDV for all the networks. 
 
-------------------------------------------------------------------------------------------------------------------------------
Libraries Required: 

    - numpy==1.17.3
    - pandas==0.25.3 
    - scipy==1.3.2
    - scikit-learn==0.21.3
    - matplotlib==3.1.2
    - matplotlib-venn==0.11.5
    - seaborn==0.11.0
    - statannot==0.2.3
    - networkx==2.4
    - statsmodels==0.10.2
    - gprofiler-official==1.0.0
    - rdkit